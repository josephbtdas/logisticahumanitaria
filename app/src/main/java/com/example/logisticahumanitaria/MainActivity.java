package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import database.DbEstados;
import database.Estados;

public class MainActivity extends AppCompatActivity
{
    private EditText edtNombre;
    private CheckBox chkFavorito;
    private Estados savedEstado;
    private int id;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.txtNombre);
        chkFavorito = (CheckBox) findViewById(R.id.ckEstado);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);



        btnGuardar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean completo = true;
                if (edtNombre.getText().toString().equals(""))
                {
                    edtNombre.setError("Introduce el nombre");
                    completo = false;
                }
                if (completo)
                {
                    DbEstados source = new DbEstados(MainActivity.this);
                    source.openDataBase();
                    Estados nContacto = new Estados();
                    nContacto.setNombre(edtNombre.getText().toString());
                    if(chkFavorito.isChecked())
                    {
                        nContacto.setStatus(1);
                    }
                    else
                    {
                        nContacto.setStatus(0);
                    }
                    if (savedEstado == null) {
                        long i = source.insertEstado(nContacto);
                        Toast.makeText(MainActivity.this,R.string.mensaje + " id= " + i ,
                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateEstado(nContacto,id);
                        Toast.makeText(MainActivity.this, R.string.mensajeedit + id,
                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                    source.closeDataBase();

                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        ListaActivity.class);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode)
        {
            Estados contacto = (Estados) data.getSerializableExtra("Estados");
            savedEstado = contacto;
            id = contacto.get_ID();
            Toast.makeText(MainActivity.this,"ID"+ id, Toast.LENGTH_SHORT).show();
            edtNombre.setText(contacto.getNombre());
            if (contacto.getStatus() > 0)
            {
                chkFavorito.setChecked(true);
            }
        }
        else
        {
            limpiar();
        }
    }
    public void limpiar()
    {
        savedEstado = null;
        edtNombre.setText("");
        edtNombre.requestFocus();
        chkFavorito.setChecked(false);
    }
}
