package com.example.logisticahumanitaria;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.DbEstados;
import database.Estados;

public class ListaActivity extends ListActivity
{
    private DbEstados dbEstados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        dbEstados = new DbEstados(this);
        dbEstados.openDataBase();
        ArrayList<Estados> Estados = dbEstados.allContactos();
        MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.activity_estado, Estados);
        setListAdapter(adapter);
        //NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    class MyArrayAdapter extends ArrayAdapter<Estados>
    {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup)
        {
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreContacto);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            if(objects.get(position).getStatus()>0){
                lblNombre.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
            }
            lblNombre.setText(objects.get(position).getNombre());
            borrar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int id = objects.get(position).get_ID();
                    String nombre = objects.get(position).getNombre();
                    //personalizado(id,nombre);
                    Toast.makeText(getApplicationContext(),"id: " + objects.get(position).get_ID(),Toast.LENGTH_SHORT).show();
                    dbEstados.openDataBase();
                    dbEstados.borrarEstados(objects.get(position).get_ID());
                    dbEstados.closeDataBase();
                    objects.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),"Se ha borrado",Toast.LENGTH_SHORT).show();

                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("Estados", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;

        }
    }


}
