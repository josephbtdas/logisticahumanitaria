package database;

import java.io.Serializable;

public class Estados implements Serializable {


    private int _ID;
    private String nombre;
    private int status;

    public Estados()
    {
        this._ID=0;
        this.nombre="";
        this.status=0;
    }
    //sdf
    public Estados(int _ID, String nombre, int status)
    {
        this._ID=_ID;
        this.nombre=nombre;
        this.status=status;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
