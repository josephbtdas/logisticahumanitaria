package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbEstados {
    Context context;
    EstadosDbHelper mDbHelper;
    SQLiteDatabase db;
    String[]columnsToRead = new String[]
            {
                    DefinirTabla.Estado._ID,
                    DefinirTabla.Estado.COLUMN_NAME_NOMBRE,
                    DefinirTabla.Estado.COLUMN_NAME_STATUS
            };

    public DbEstados(Context context)
    {
        this.context = context;
        mDbHelper = new EstadosDbHelper(this.context);
    }

    public void openDataBase()
    {
        db = mDbHelper.getWritableDatabase();
    }

    public long insertEstado(Estados e)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, e.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_STATUS,e.getStatus());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);//regresa el id insertado
    }
    public long updateEstado(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_STATUS, c.getStatus());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estado.TABLE_NAME , values,
                DefinirTabla.Estado._ID + " = " + id,null);
    }
    public int borrarEstados(long id)
    {
        return db.delete(DefinirTabla.Estado.TABLE_NAME,
                DefinirTabla.Estado._ID + "=?",
                new String[]{ String.valueOf(id) });
    }

    private Estados leerEstado(Cursor cursor)
    {
        Estados c = new Estados();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setStatus(cursor.getInt(2));
        return c;

    }

    public void leerEstados(int id)
    {

    }

    public Estados getContacto(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnsToRead,

                DefinirTabla.Estado._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados estado = leerEstado(c);
        c.close();
        return estado;
    }

    public ArrayList<Estados> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnsToRead, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = leerEstado(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
    public void closeDataBase()
    {
        mDbHelper.close();
    }
}
